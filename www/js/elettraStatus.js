function loadElettraStatus(){
	var title="Elettra Status";
	if(app) {
		remember = document.forms["rememberMode"]["checkbox"].checked;
		if(remember){
			aggLastMode(title);
		} else {
			localStorage.setItem("lastMode","");
		}
	}
	$("#mainPage").load("elettraStatus.html", function(responseText,textStatus,jqXHR) {
		if(textStatus=="error") {
			$("#mainPage").html('<div class="media bg-info"> <div class="media-left media-middle"> <img src="img/icon.png" class="media-object" style="width:40px" /> </div> <div class="media-body media-middle"> <h4 class="text-center center-block media-heading" >Elettra Status</h4></div></div><hr><div id="networkErr" class="container-fluid" style="display:none"><h4 class="text-danger text-center">Error: not possible to load the page</h4></div>');
		} else {
			
			adaptToWidthElettra();
			$(window).on("orientationchange",iframeElettraRefresh);
			
			$(document).ready( function() {
				$(".media-left").html('<a onclick="localStorage.setItem(\'lastMode\',\'\')" href="index.html"> <img src="img/icon.png" class="media-object" style="width:40px" /></a>');
				$.when( aggElettraLente() )
				 // .done( function() { aggElettraRapide(fel);})
				 .fail( function () {
					 navigator.notification.alert("Aggiornamento fallito", null, "Errore");
					//aggElettraRapide(fel)
					});
			});
		}
	});
}

function iframeElettraRefresh() {
	if(app) {
		window.cache.clear( cacheClear, cacheNotClear );
		window.cache.cleartemp();
	}
	adaptToWidthElettra();
	// $("#intframe").prop("src",$("#intframe").prop("src"));
	$("#rmsframe").prop("src",$("#rmsframe").prop("src"));
};

function adaptToWidthElettra() {
	if($(window).width()>=768) {
		$("#statusBtn").click();
		$("#crBtn").click();
	}
	/*
	if($(window).width()>=768) {
		if($("#intframe").prop("src").indexOf("last%202%20hours")>-1) {
			// $("#intframe").prop("src",$("#intframe").prop("src").split("start=last%202%20hours")[0]+"start=last%2024%20hours"+$("#intframe").prop("src").split("start=last%202%20hours")[1]);
			$("#rmsframe").prop("src",$("#rmsframe").prop("src").split("start=last%202%20hours")[0]+"start=last%2024%20hours"+$("#rmsframe").prop("src").split("start=last%202%20hours")[1]);
			$("#monitorTitle").html($("#monitorTitle").html().split("(last 2 hours)")[0]+"(last 24 hours)");
		}
		if(!($("#collapse1").prop("class").indexOf("in")>-1)) { $("#statusBtn").click(); }
		if(!($("#collapse2").prop("class").indexOf("in")>-1)) { $("#linacBtn").click(); }
		if(!($("#collapse3").prop("class").indexOf("in")>-1)) { $("#seedBtn").click(); }
		if(!($("#collapse4").prop("class").indexOf("in")>-1)) { $("#felBtn").click(); }
	} else {
		if($("#intframe").prop("src").indexOf("last%2024%20hours")>-1) {
			// $("#intframe").prop("src",$("#intframe").prop("src").split("start=last%2024%20hours")[0]+"start=last%202%20hours"+$("#intframe").prop("src").split("start=last%2024%20hours")[1]);
			$("#rmsframe").prop("src",$("#rmsframe").prop("src").split("start=last%2024%20hours")[0]+"start=last%202%20hours"+$("#rmsframe").prop("src").split("start=last%2024%20hours")[1]);
			$("#monitorTitle").html($("#monitorTitle").html().split("(last 24 hours)")[0]+"(last 2 hours)");
		}
	}
	*/
};

function aggElettraLente(){
	return $.when(
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=sr/DIAGNOSTICS/DCCT_S4/Current","current",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=sr/DIAGNOSTICS/DCCT_S4/Energy","energy",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=sr/DIAGNOSTICS/DCCT_S4/LifetimeHours","lifeTime",":"),
		httpGetInformationSystem(),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=elettra/status/userstatus/machinestatus","machineInfo",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=elettra/status/userstatus/operationmode","operationMode",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=elettra/status/userstatus/bunchmode","bunchMode",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=elettra/status/userstatus/lastudinjon","lastUserDedicatedInjectionDate",":"),
		httpGetLastUserDedicatedInjectionTime("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=elettra/status/userstatus/lastudinjtime","lastUserDedicatedInjectionTime",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=elettra/status/userstatus/controlroom","controlRoom",":"),
		lastUserDedicatedInjectionDateTime(),
		changeColor()
	).done( function() {
		
		if (app || $_GET.version=="app"){
			$("#infoUp").val(new Date().toLocaleString());
			// $("#intframe").prop("src",$("#intframe").prop("src"));
			$("#rmsframe").prop("src",$("#rmsframe").prop("src"));
		} else {
				var date = new Date().toLocaleString();
				document.getElementById("infoUp").value = date;	
				
			// $("#infoUp").html(new Date().toLocaleString());
			// $("#intimg").prop("src",$("#intimg").prop("src"));
			// $("#rmsimg").prop("src",$("#rmsimg").prop("src"));
		}
		var t = setTimeout(function() { aggElettraLente(); },8000);
	})
	.fail( function() {
		var t = setTimeout(function() { aggElettraLente(); }, 10000);
	})
};

function aggElettraRapide(felAc){
	/*
	$.when(
	
		//E-beam Energy
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/fermistatus/e_beam_energy","eBeamEn",":"),
	
		//Bunch Charge
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=lh/diagnostics/cm_lh.01/Charge","bunchCharge",":"),
		
		//Pulse length
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/SL_pulse_length","pulseLength",":"),

		/*
		//Intensity chart
		$("#intimg").prop("src",$("#intimg").attr("src").split("?")[0]+"?time="+new Date().getTime()),
		
		//RMS chart
		$("#rmsimg").prop("src",$("#rmsimg").attr("src").split("?")[0]+"?time="+new Date().getTime()),
		* /
		
		).done(function() {
			if(app || $_GET.version=="app") {
				$("#infoUp").val(new Date().toLocaleString());
			} else {
				$("#infoUp").html(new Date().toLocaleString());
			}
			var t = setTimeout(function() { aggElettraRapide(felAc); },2000);
		})
		.fail( function() {
		var t= setTimeout(function() { aggElettraRapide(felAc); }, 5000);
	})
	*/
};

function httpGetInformationSystem(){
	var url = 'http://ecsproxy.elettra.eu/docs/fermi/app/information_system.php';
	var id = 'informationSystem';
	return $.get(url)
		.done(function(data) {
			if (app || $_GET.version=="elettraStatus") $("#"+id).val(data); else $("#"+id).html(data);
		})
		.fail(function() {
			if (app || $_GET.version=="elettraStatus") {
				$("#"+id).val("HTTP error");
			} else {
				$("#"+id).html("HTTP error");
			}
		})
};

function httpGetLastUserDedicatedInjectionTime(url,id,sep,encoded,debug){
	return $.get(url)
		.done(function(data) {
			if (debug==1) console.log(url+' '+id+' '+sep+' ---> ',data);
			if (data.toLowerCase().indexOf("attributo")>-1) {
				$("#"+id).val("PHP parameters error");
			} else if (data.toLowerCase().indexOf("socket")>-1 || data.toLowerCase().indexOf("device")>-1) {
				$("#"+id).val("----");
			} else {
				var x = data.split(sep)[0] + ':' + data.split(sep)[1] ;
				if (!encoded) {
					if (!isNaN(x)) {
						x=Math.round(1000*x)/1000;
					}
					if (debug==1) console.log($("#"+id).val()+' VS '+$("#"+id).html());
					if (app || $_GET.version=="app" || $_GET.version=="elettraStatus" || $_GET.version=="scwStatus") $("#"+id).val(x); else $("#"+id).html(x);
				}
				else {
					$("#"+id).val((typeof encoded[x] !== 'undefined')? encoded[x]: 'undefined');
				}
			}
		})
		.fail(function() {
			if (app) {
				$("#"+id).val("HTTP error");
			} else {
				$("#"+id).html("HTTP error");
			}
		})
};	
										
function lastUserDedicatedInjectionDateTime(){
	 document.getElementById("lastUserDedicatedInjectionDateTime").innerHTML = "ciao";
};

function changeColor(url) {
	$.get("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=sr/DIAGNOSTICS/DCCT_S4/Current")
	.done(function(data) {
		current = data.split(":")[0];
		if(current < 10) {
			$("#statusBtn").removeClass('btn-primary').addClass('btn-danger');
		}
	})
	$.get("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=elettra/status/userstatus/machinestatus")
	.done(function(data) {
		machineStatus = data.split(":")[0];
		if(machineStatus == 0) {
			$("#statusBtn").removeClass('btn-primary').addClass('btn-warning');
			$("#crBtn").removeClass('btn-primary').addClass('btn-warning');
		}
	})
};