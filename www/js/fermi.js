function loadFERMI(){
console.log('loadFERMI');
	var title="FERMI Status";
	if(app) {
		remember = document.forms["rememberMode"]["checkbox"].checked;
		if(remember){
			aggLastMode(title);
		} else {
			localStorage.setItem("lastMode","");
		}
	}
	$("#mainPage").load("fermiIndex.html", function(responseText,textStatus,jqXHR) {
		if(textStatus=="error") {
			$("#mainPage").html('<div class="media bg-info"> <div class="media-left media-middle"> <img src="img/icon.png" class="media-object" style="width:40px" /> </div> <div class="media-body media-middle"> <h4 class="text-center center-block media-heading" >FERMI Status</h4></div></div><hr><div id="networkErr" class="container-fluid" style="display:none"><h4 class="text-danger text-center">Error: not possible to load the page</h4></div>');
		} else {
			
			adaptToWidth();
			$(window).on("orientationchange",iframeRefresh);
			
			$(document).ready( function() {
				$(".media-left").html('<a onclick="localStorage.setItem(\'lastMode\',\'\')" href="index.html"> <img src="img/icon.png" class="media-object" style="width:40px" /></a>');
				$.when( aggLente(fel) )
				 .done( function() { aggRapide(fel);})
				 .fail( function () {
					 navigator.notification.alert("Aggiornamento valori principali fallito.", null, "Errore");
					aggRapide(fel)});
			});
		}
	});
}

function iframeRefresh() {
	if(app) {
		window.cache.clear( cacheClear, cacheNotClear );
		window.cache.cleartemp();
	}
	adaptToWidth();
	$("#i0mintframe").prop("src",$("#i0mintframe").prop("src"));
	$("#i0mrmsframe").prop("src",$("#i0mrmsframe").prop("src"));
};

function adaptToWidth() {
	if($(window).width()>=768) {
		if($("#i0mintframe").prop("src").indexOf("last%202%20hours")>-1) {
			$("#i0mintframe").prop("src",$("#i0mintframe").prop("src").split("start=last%202%20hours")[0]+"start=last%2024%20hours"+$("#i0mintframe").prop("src").split("start=last%202%20hours")[1]);
			$("#i0mrmsframe").prop("src",$("#i0mrmsframe").prop("src").split("start=last%202%20hours")[0]+"start=last%2024%20hours"+$("#i0mrmsframe").prop("src").split("start=last%202%20hours")[1]);
			$("#monitorTitle").html($("#monitorTitle").html().split("(last 2 hours)")[0]+"(last 24 hours)");
		}
		if(!($("#collapse1").prop("class").indexOf("in")>-1)) { $("#statusBtn").click(); }
		if(!($("#collapse2").prop("class").indexOf("in")>-1)) { $("#linacBtn").click(); }
		if(!($("#collapse3").prop("class").indexOf("in")>-1)) { $("#seedBtn").click(); }
		if(!($("#collapse4").prop("class").indexOf("in")>-1)) { $("#felBtn").click(); }
	} else {
		if($("#i0mintframe").prop("src").indexOf("last%2024%20hours")>-1) {
			$("#i0mintframe").prop("src",$("#i0mintframe").prop("src").split("start=last%2024%20hours")[0]+"start=last%202%20hours"+$("#i0mintframe").prop("src").split("start=last%2024%20hours")[1]);
			$("#i0mrmsframe").prop("src",$("#i0mrmsframe").prop("src").split("start=last%2024%20hours")[0]+"start=last%202%20hours"+$("#i0mrmsframe").prop("src").split("start=last%2024%20hours")[1]);
			$("#monitorTitle").html($("#monitorTitle").html().split("(last 24 hours)")[0]+"(last 2 hours)");
		}
	}
};

function felActive(felAc) {
	if (app || $_GET.version=="app") {
		return $.get("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/access_control/safety/Undulator_access_state")
			.done(function(data) {
				if(data.toLowerCase().indexOf("error")>-1) {
					$("#felActive").val("PHP parameters error");
				} else {
					var arr=data.split(",");
					if(arr[5]=="True") {
						felAc[0]=true;
						felAc[1]=false;
						$("#felActive").val("FEL-1");
					} else if (arr[6]=="True") {
						felAc[0]=false;
						felAc[1]=true;
						$("#felActive").val("FEL-2");
					} else {
						felAc[0]=false;
						felAc[1]=false;
						$("#felActive").val("None");
					}
				}
			})
			.fail(function() {
				$("#felActive").val("HTTP error");
			})
	} else {
		return $.get("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/access_control/safety/Undulator_access_state")
			.done(function(data) {
				if(data.toLowerCase().indexOf("error")>-1) {
					$("#felActive").html("PHP parameters error");
				} else {
					var arr=data.split(",");
					if(arr[5]=="True") {
						felAc[0]=true;
						felAc[1]=false;
						$("#felActive").html("FEL-1");
					} else if (arr[6]=="True") {
						felAc[0]=false;
						felAc[1]=true;
						$("#felActive").html("FEL-2");
					} else {
						felAc[0]=false;
						felAc[1]=false;
						$("#felActive").html("None");
					}
				}
			})
			.fail(function() {
				$("#felActive").html("HTTP error");
			})
	}
};

function aggLente(felAc){
	
	return $.when(
		//Beam to
		// httpGetFermi("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=beamto","beamTo"),
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=beamto","beamTo"),
		
		//RF Plants
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=webinterface/rf_plants","rfPlants"),
			
		//Bc-1/Energy
		($.when(httpGetFermi("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=bc01/power_supply/psb_bc01.01/state","bc1En",":"))
			.done(function() { httpGetFermi("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/bc01_energy","bc1En",":"); })),
		
		//Bc-2/Energy
		($.when(httpGetFermi("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=bc02/power_supply/psb_bc02.01/state","bc2En",":"))
			.done(function() { httpGetFermi("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/bc02_energy","bc2En",":"); })),
		
		//Seed Laser Status
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/fermistatus/seed_laser_status","slStatus",":",{0:"----",3:"----",7:"----",2:"OPEN",4:"OPEN",6:"OPEN",1:"ATTENUATING",5:"CLOSED"}),

		//Locking
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=sl/timing/tmu_sl.01/LockStatus","locking",":"),
		
		//Fel Active
		felActive(felAc)
		
	).done( function() {
		
		if (felAc[0]) {
			
			$("#stage1title").hide();
			$("#secondStage").hide();
			$("#felBtn").html("FEL-1");
			$("#felData").show("slow");
			$("#felMonitor").html("FEL-1 I<sub>0 </sub> monitor");
			if(app || $_GET.version=="app") {
				if($("#i0mintframe").prop("src").indexOf("ts=02078")>-1) {
					$("#i0mintframe").prop("src",$("#i0mintframe").prop("src").split("ts=02078")[0]+"ts=02076"+$("#i0mintframe").prop("src").split("ts=02078")[1]);
					$("#i0mrmsframe").prop("src",$("#i0mrmsframe").prop("src").split("ts=02243")[0]+"ts=02241"+$("#i0mrmsframe").prop("src").split("ts=02243")[1]);
				}
				$("#monitorTitle").html("FEL-1 I<sub>0</sub> monitor charts (last 2 hours)");
				$("#firstStage").prop("class","col-xs-12");
				$("#felMonitor").prop("class","text-primary text-center");
			} else {
				$("#noContent").hide("slow");
				$("#monitorTitle").html("FEL-1 I<sub>0</sub> monitor charts (30 minutes)");
				$("#firstStage").css("width","100%");
				$("#felMonitor").css("text-align","center");
			}
			
		} else if(felAc[1]){
			
			if (app || $_GET.version=="app") {
				if($("#i0mintframe").prop("src").indexOf("ts=02076")>-1) {
					$("#i0mintframe").prop("src",$("#i0mintframe").prop("src").split("ts=02076")[0]+"ts=02078"+$("#i0mintframe").prop("src").split("ts=02076")[1]);
					$("#i0mrmsframe").prop("src",$("#i0mrmsframe").prop("src").split("ts=02241")[0]+"ts=02243"+$("#i0mrmsframe").prop("src").split("ts=02241")[1]);
				}
				$("#monitorTitle").html("FEL-2 I<sub>0</sub> monitor charts (last 2 hours)");
				$("#felMonitor").prop("class","text-primary text-right");
				$("#firstStage").prop("class","col-xs-6");
			} else {
				$("#noContent").hide("slow");
				$("#monitorTitle").html("FEL-2 I<sub>0</sub> monitor charts (30 minutes)");
				$("#felMonitor").css("text-align","right");
				$("#firstStage").css("width","50%");
			}
			$("#felMonitor").html("FEL-2 I<sub>0 </sub>");
			$("#felBtn").html("FEL-2");
			$("#stage1title").show();
			$("#secondStage").show();
			$("#felData").show("slow");
			
		} else {
			$("#noContent").show("slow");
			$("#felData").hide("slow");
			
		}
		if (app || $_GET.version=="app"){
			$("#infoUp").val(new Date().toLocaleString());
			$("#i0mintframe").prop("src",$("#i0mintframe").prop("src"));
			$("#i0mrmsframe").prop("src",$("#i0mrmsframe").prop("src"));
		} else {
			$("#infoUp").html(new Date().toLocaleString());
			$("#intimg").prop("src",$("#intimg").prop("src"));
			$("#rmsimg").prop("src",$("#rmsimg").prop("src"));
		}
		var t = setTimeout(function() { aggLente(felAc); },60000);
	})
	.fail( function() {
		var t = setTimeout(function() { aggLente(felAc); }, 10000);
	})
	
};

function aggRapide(felAc){
	
	$.when(
	
		//E-beam Energy
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/fermistatus/e_beam_energy","eBeamEn",":"),
	
		//Bunch Charge
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=lh/diagnostics/cm_lh.01/Charge","bunchCharge",":"),
		
		//Pulse length
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/SL_pulse_length","pulseLength",":"),

		/*
		//Intensity chart
		$("#intimg").prop("src",$("#intimg").attr("src").split("?")[0]+"?time="+new Date().getTime()),
		
		//RMS chart
		$("#rmsimg").prop("src",$("#rmsimg").attr("src").split("?")[0]+"?time="+new Date().getTime()),
		*/
		
		//Aggiornamento dati FEL
		aggFEL(felAc)
		
		).done(function() {
			if(app || $_GET.version=="app") {
				$("#infoUp").val(new Date().toLocaleString());
			} else {
				$("#infoUp").html(new Date().toLocaleString());
			}
			var t = setTimeout(function() { aggRapide(felAc); },2000);
		})
		.fail( function() {
		var t= setTimeout(function() { aggRapide(felAc); }, 5000);
	})
};

function aggFEL(felAct) {
	
		var d;
		
		if(felAct[0]) {
			
			d = $.when(
				
				//Pulse Energy
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/fermistatus/seed_laser_pulse_energy","pulseEn",":"),
				
				//Wavelength
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/SL01_wavelength","wavelength",":"),
		
				//Spectrometer wavelength
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pos/pesp/ccd-pesp_pos.01/SpectrumPeakPos_d","waveSpec",":"),
		
				//Spectrometer bandwidth
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pos/pesp/ccd-pesp_pos.01/SpectrumFWHM_d","bandSpec",":"),
		
				//Harmonic FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL01_harmonic_number","harmfel1",":"),
				
				//Wavelength FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL01_wavelength","wavefel1",":"),
				
				//Polarization FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=FEL01/master/master_FEL01.01/polarization","polfel1",":",{0:"N.A.",1:"Linear Vertical",2:"Linear Horizontal",3:"Right circular",4:"Left circular"}),
				
				//Intensity FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f01/diagnostics/iom_pfe_f01.01/IAvg","intfel1",":"),
				
				//RMS FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f01/diagnostics/iom_pfe_f01.01/IStd","rmsfel1",":")
			);
		} else if(felAct[1]){
			d = $.when(
				
				//Pulse Energy
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=sl/energy_meter/emcalib_sl.02/energy","pulseEn",":"),
		
				//Wavelength
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/SL02_wavelength","wavelength",":"),
				
				//Spectrometer wavelength
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pos/pesp/ccd-pesp_pos.01/SpectrumPeakPos_d","waveSpec",":"),
			
				//Spectrometer bandwidth
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pos/pesp/ccd-pesp_pos.01/SpectrumFWHM_d","bandSpec",":"),
			
				//Harmonic FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL02_FirstStage_harmonic_number","harmfel1",":"),
				
				//Wavelength FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL02_FirstStage_wavelength","wavefel1",":"),
				
				//Polarization FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=FEL02/master_s01/master_s01_FEL02.01/polarization","polfel1",":",{0:"N.A.",1:"Linear Vertical",2:"Linear Horizontal",3:"Right circular",4:"Left circular"}),
				
				//Intensity FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f02/diagnostics/iom_pfe_f02.01/IAvg","intfel1",":"),
				
				//RMS FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f02/diagnostics/iom_pfe_f02.01/IStd","rmsfel1",":"),
				
				//Harmonic FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL02_SecondStage_harmonic_number","harmfel2",":"),
				
				//Wavelength FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL02_SecondStage_wavelength","wavefel2",":"),
				
				//Polarization FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=FEL02/master_s02/master_s02_FEL02.01/polarization","polfel2",":",{0:"N.A.",1:"Linear Vertical",2:"Linear Horizontal",3:"Right circular",4:"Left circular"}),
				
				//Intensity FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f02/diagnostics/iom_pfe_f02.02/IAvg","intfel2",":"),
				
				//RMS FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f02/diagnostics/iom_pfe_f02.02/IStd","rmsfel2",":")
			);
		} else {
			d=$.Deferred();
			$("#felData").hide("slow");
			d.resolve();
		}
		return d;
};

function httpGetFermi(url,id,sep){
	return $.get(url)
		.done(function(data) {
			if (app || $_GET.version=="app") {
				if (data.toLowerCase().indexOf("attributo")>-1) {
					$("#"+id).val("PHP parameters error");
				} else if (data.toLowerCase().indexOf("socket")>-1 || data.toLowerCase().indexOf("device")>-1) {
					$("#"+id).val("----");
				} else {
					var x = data.split(sep)[0];
					switch (id){
						case "bc1En":
						case "bc2En":
								if(x=="FAULT") {
									$("#"+id).val("OFF");
									break;
								}
								if($("#"+id).val().indexOf("ON")>-1) {
									if (url.indexOf("energy")>-1 ) {
										$("#"+id).val("ON / "+x+" GeV");
										break;
									} else {
										$("#"+id).val(x);
										break;
									}
								} else {
									if (url.indexOf("energy")>-1 ) {
										break;
									} else {
										$("#"+id).val(x);
										break;
									}
								}
						default:
							if (!isNaN(x)) {
								x=Math.round(1000*x)/1000;
							}
							$("#"+id).val(x);
					}
				}
			} else {
				
				if (data.toLowerCase().indexOf("attributo")>-1) {
					$("#"+id).hmtl("PHP parameters error");
				} else if (data.toLowerCase().indexOf("socket")>-1 || data.toLowerCase().indexOf("device")>-1) {
					$("#"+id).html("----");
				} else {
					var x = data.split(sep)[0];
					switch (id){
						case "bc1En":
						case "bc2En":
								if(x=="FAULT") {
									$("#"+id).html("OFF");
									break;
								}
								if($("#"+id).html().indexOf("ON")>-1) {
									if (url.indexOf("energy")>-1 ) {
										$("#"+id).html("ON / "+x+" GeV");
										break;
									} else {
										$("#"+id).html(x);
										break;
									}
								} else if($("#"+id).html().indexOf("OFF")>-1){
									if (url.indexOf("energy")>-1 ) {
										break;
									} else {
										$("#"+id).html(x);
										break;
									}
								}
						default:
							if (!isNaN(x)) {
								x=Math.round(1000*x)/1000;
							}
							$("#"+id).html(x);
					}
				}
			}
		})
		.fail(function() {
			if (app) {
				$("#"+id).val("HTTP error");
			} else {
				$("#"+id).html("HTTP error");
			}
		})
};
