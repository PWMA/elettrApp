$("document").ready( function aggiornamento() {
	
	var fel1=false;
	var fel2=false;
	
	//Aspetto di aver aggiornato tutti i dati prima di settare il timer per chiamare nuovamente questa funzione
	$.when(
		//Beam to
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=beamto","beamTo"),
		/*
		//Beam Line
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pos/posmaster/pathto/beamline","beamTo"),
		*/
		//RF Plants
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/modulators/modulators/kgglobalstat","rfPlants",":"),
		
		//E-beam Energy
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/fermistatus/e_beam_energy","eBeamEn",":"),
		
		//Bc-1/Energy
		($.when(httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=bc01/power_supply/psb_bc01.01/state","bc1En",":"))
			.done(httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/bc01_energy","bc1En",":"))),
		
		//Bc-2/Energy
		($.when(httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=bc02/power_supply/psb_bc02.01/state","bc2En",":"))
			.done(httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/bc02_energy","bc2En",":"))),

		//Bunch Charge
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=lh/diagnostics/cm_lh.01/Charge","bunchCharge",":"),
		
		//Seed Laser Status
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/fermistatus/seed_laser_status","slStatus",":"),

		//Locking
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=sl/timing/tmu_sl.01/LockStatus","locking",":"),
		
		//Pulse Energy
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/fermistatus/seed_laser_pulse_energy","pulseEn",":"),
		
		//Pulse length
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/SL_pulse_length","pulseLength",":"),

		//Wavelength
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/SL01_wavelength","wavelength",":"),

		//Aspetto di sapere quale FEL e' attivo, per visualizzare i dati di conseguenza
		$.when(
			//Fel Active
			$.get("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/access_control/safety/Undulator_access_state")
				.done(function(data) {
					if(data.toLowerCase().includes("error")) {
						$("#felActive").val("PHP parameters error");
					} else {
						var arr=data.split(",");
						if(arr[5]=="True") {
							fel1=true;
							$("#felActive").val("FEL-1");
						} else if (arr[6]=="True") {
							fel2=true;
							$("#felActive").val("FEL-2");
						} else {
							$("#felActive").val("None");
						}
					}
				})
				.fail(function() {
					$("#felActive").val("HTTP error");
				})
		)/*.done( function() {
			//Intensity chart
			$("#intimg").prop("src",$("#intimg").attr("src").split("?")[0]+"?time="+new Date().getTime());
			//RMS chart
			$("#rmsimg").prop("src",$("#rmsimg").attr("src").split("?")[0]+"?time="+new Date().getTime());
		})*/
		.done( function() {
			
			//Spectrometer wavelength
			httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pos/pesp/ccd-pesp_pos.01/SpectrumPeakPos_d","waveSpec",":");
			
			//Spectrometer bandwidth
			httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pos/pesp/ccd-pesp_pos.01/SpectrumFWHM_d","bandSpec",":");
			
			if(fel1) {
				$("#felBtn").html("FEL-1");
				$("#secondStage").hide();
				$("#stage1title").hide();
				$("#firstStage").prop("class","col-xs-12");
				$("#firstStage").animate({"padding-right":"15px"});
				$("#felMonitor").prop("class","text-primary text-center");
				$("#felMonitor").html("FEL-1 I<sub>0 </sub> monitor");
				
				$("#felBtn").prop("class", "btn btn-primary btn-block");
				//Harmonic FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL01_harmonic_number","harmfel1",":");
				
				//Wavelength FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL01_wavelength","wavefel1",":");
				
				//Polarization FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=FEL01/master/master_FEL01.01/polarization","polfel1",":");
				
				//Intensity FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f01/diagnostics/iom_pfe_f01.01/IAvg","intfel1",":");
				
				//RMS FEL-1
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f01/diagnostics/iom_pfe_f01.01/IStd","rmsfel1",":");
			
			} else if(fel2){
				$("#felMonitor").html("FEL-2 I<sub>0 </sub>");
				$("#felMonitor").prop("class","text-primary text-right");
				$("#felBtn").html("FEL-2");
				$("#firstStage").prop("class","col-xs-6");
				$("#firstStage").animate({"padding-right":"0"});
				$("#stage1title").show();
				$("#secondStage").show();
				
				$("#felBtn").prop("class", "btn btn-primary btn-block");
				//Harmonic FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL02_FirstStage_harmonic_number","harmfel1",":");
				
				//Wavelength FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL02_FirstStage_wavelength","wavefel1",":");
				
				//Polarization FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=FEL02/master_s01/master_s01_FEL02.01/polarization","polfel1",":");
				
				//Intensity FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f02/diagnostics/iom_pfe_f02.01/IAvg","intfel1",":");
				
				//RMS FEL-2 first stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f02/diagnostics/iom_pfe_f02.01/IStd","rmsfel1",":");
				
				//Harmonic FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL02_SecondStage_harmonic_number","harmfel2",":");
				
				//Wavelength FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/FEL02_SecondStage_wavelength","wavefel2",":");
				
				//Polarization FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=FEL02/master_s02/master_s02_FEL02.01/polarization","polfel2",":");
				
				//Intensity FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f02/diagnostics/iom_pfe_f02.02/IAvg","intfel2",":");
				
				//RMS FEL-2 second stage
				httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=pfe_f02/diagnostics/iom_pfe_f02.02/IStd","rmsfel2",":");
			} else {
				$("#felData").hide("slow");
			}
		})
	).done( function()  {
		$("#infoUp").val(new Date().toUTCString());
		var t = setTimeout(aggiornamento,10000);
	});
});

function httpGet(url,id,sep){
	return $.get(url)
		.done(function(data) {
			
			if (data.toLowerCase().includes("attributo")) {
				$("#"+id).val("PHP parameters error");
			} else if (data.toLowerCase().includes("socket") || data.toLowerCase().includes("device")) {
				$("#"+id).val("----");
			} else {
				var x = data.split(sep)[0];
				switch (id){
					case "beamTo":
						$("#"+id).val(data.split("- ")[1]);
						break;
					case "rfPlants":
						if (x=="1"){
							$("#"+id).val("ON");
						} else {
							$("#"+id).val("OFF");
						}
						break;
					case "polfel1":
					case "polfel2":
						switch (x){
							case "0":
								$("#"+id).val("N.A.");
								break;
							case "1":
								$("#"+id).val("Linear Vertical");
								break;
							case "2":
								$("#"+id).val("Linear Horizontal");
								break;
							case "3":
								$("#"+id).val("Right circular");
								break;
							case "4":
								$("#"+id).val("Left circular");
								break;
							default:
								$("#"+id).val("Not Defined");
						}
						break;
					case "slStatus":
						switch (x){
							case "0":
							case "3":
							case "7":
								$("#"+id).val("----");
								break;
							case "2":
							case "4":
							case "6":
								$("#"+id).val("OPEN");
								break;
							case "1":
								$("#"+id).val("ATTENUATING");
								break;
							case "5":
								$("#"+id).val("CLOSED");
								break;
							default:
								$("#"+id).val("Not Defined");
						}
						break;
					case "bc1En":
					case "bc2En":
							if($("#"+id).val().includes("ON")) {
								if (url.includes("energy") ) {
									$("#"+id).val("ON/ "+(Number(x)*1000)+"MeV");
									break;
								} else {
									$("#"+id).val(x);
								}
							} else if ($("#"+id).val()=="OFF") {
								if (url.includes("energy") ) {
									break;
								} else {
									$("#"+id).val(x);
								}
							}
					default:
						$("#"+id).val(x);
				}
			}
		})
		.fail(function() {
			$("#"+id).val("HTTP error");
		})
}