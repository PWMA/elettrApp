function loadScwStatus(){
	var title="Scw Status";
	if(app) {
		remember = document.forms["rememberMode"]["checkbox"].checked;
		if(remember){
			aggLastMode(title);
		} else {
			localStorage.setItem("lastMode","");
		}
	}
	$("#mainPage").load("scwStatus.html", function(responseText,textStatus,jqXHR) {
		if(textStatus=="error") {
			$("#mainPage").html('<div class="media bg-info"> <div class="media-left media-middle"> <img src="img/icon.png" class="media-object" style="width:40px" /> </div> <div class="media-body media-middle"> <h4 class="text-center center-block media-heading" >SCW Status</h4></div></div><hr><div id="networkErr" class="container-fluid" style="display:none"><h4 class="text-danger text-center">Error: not possible to load the page</h4></div>');
		} else {
			
			adaptToWidthScw();
			$(window).on("orientationchange",iframeScwRefresh);
			
			$(document).ready( function() {
				$(".media-left").html('<a onclick="localStorage.setItem(\'lastMode\',\'\')" href="index.html"> <img src="img/icon.png" class="media-object" style="width:40px" /></a>');
				$.when( aggScwLente() )
				 // .done( function() { aggScwRapide(fel);})
				 .fail( function () {
					 navigator.notification.alert("Aggiornamento fallito", null, "Errore");
					//aggScwRapide(fel)
					});
			});
		}
	});
}

function iframeScwRefresh() {
	if(app) {
		window.cache.clear( cacheClear, cacheNotClear );
		window.cache.cleartemp();
	}
	adaptToWidthScw();
	$("#intframe").prop("src",$("#intframe").prop("src"));
	$("#rmsframe").prop("src",$("#rmsframe").prop("src"));
};

function adaptToWidthScw() {
	if($(window).width()>=768) {
		$("#statusBtn").click();
		$("#crBtn").click();
	}
	/*
	if($(window).width()>=768) {
		if($("#intframe").prop("src").indexOf("last%202%20hours")>-1) {
			$("#intframe").prop("src",$("#intframe").prop("src").split("start=last%202%20hours")[0]+"start=last%2024%20hours"+$("#intframe").prop("src").split("start=last%202%20hours")[1]);
			$("#rmsframe").prop("src",$("#rmsframe").prop("src").split("start=last%202%20hours")[0]+"start=last%2024%20hours"+$("#rmsframe").prop("src").split("start=last%202%20hours")[1]);
			$("#monitorTitle").html($("#monitorTitle").html().split("(last 2 hours)")[0]+"(last 24 hours)");
		}
		if(!($("#collapse1").prop("class").indexOf("in")>-1)) { $("#statusBtn").click(); }
		if(!($("#collapse2").prop("class").indexOf("in")>-1)) { $("#linacBtn").click(); }
		if(!($("#collapse3").prop("class").indexOf("in")>-1)) { $("#seedBtn").click(); }
		if(!($("#collapse4").prop("class").indexOf("in")>-1)) { $("#felBtn").click(); }
	} else {
		if($("#intframe").prop("src").indexOf("last%2024%20hours")>-1) {
			$("#intframe").prop("src",$("#intframe").prop("src").split("start=last%2024%20hours")[0]+"start=last%202%20hours"+$("#intframe").prop("src").split("start=last%2024%20hours")[1]);
			$("#rmsframe").prop("src",$("#rmsframe").prop("src").split("start=last%2024%20hours")[0]+"start=last%202%20hours"+$("#rmsframe").prop("src").split("start=last%2024%20hours")[1]);
			$("#monitorTitle").html($("#monitorTitle").html().split("(last 24 hours)")[0]+"(last 2 hours)");
		}
	}
	*/
};

function aggScwLente(){
	return $.when(
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=sr/scw/scw_a11.1/MagneticField","magneticField",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=sr/scw/jb_a11.1/HeliumLevel","heliumLevel",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=sr/DIAGNOSTICS/DCCT_S4/Current","current",":"),
		httpGet("http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=elettra/status/userstatus/machinestatus","machineInfo",":")
	).done( function() {
		
		if (app || $_GET.version=="app"){
			$("#infoUp").val(new Date().toLocaleString());
			$("#intframe").prop("src",$("#intframe").prop("src"));
			$("#rmsframe").prop("src",$("#rmsframe").prop("src"));
		} else {
				var date = new Date().toLocaleString();
				document.getElementById("infoUp").value = date;
			// $("#infoUp").html(new Date().toLocaleString());
			// $("#intimg").prop("src",$("#intimg").prop("src"));
			// $("#rmsimg").prop("src",$("#rmsimg").prop("src"));
		}
		var t = setTimeout(function() { aggScwLente(); },8000);
	})
	.fail( function() {
		var t = setTimeout(function() { aggScwLente(); }, 10000);
	})
	
};

function aggScwRapide(felAc){
	/*
	$.when(
	
		//E-beam Energy
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/fermistatus/e_beam_energy","eBeamEn",":"),
	
		//Bunch Charge
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=lh/diagnostics/cm_lh.01/Charge","bunchCharge",":"),
		
		//Pulse length
		httpGet("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/misc/beam_status_f/SL_pulse_length","pulseLength",":"),

		/*
		//Intensity chart
		$("#intimg").prop("src",$("#intimg").attr("src").split("?")[0]+"?time="+new Date().getTime()),
		
		//RMS chart
		$("#rmsimg").prop("src",$("#rmsimg").attr("src").split("?")[0]+"?time="+new Date().getTime()),
		* /
		
		).done(function() {
			if(app || $_GET.version=="app") {
				$("#infoUp").val(new Date().toLocaleString());
			} else {
				$("#infoUp").html(new Date().toLocaleString());
			}
			var t = setTimeout(function() { aggElettraRapide(felAc); },2000);
		})
		.fail( function() {
		var t= setTimeout(function() { aggElettraRapide(felAc); }, 5000);
	})
	*/
};
