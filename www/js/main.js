var remember;
if (document.domain.indexOf('elettra.trieste.it') !== -1) {
	document.location = document.location.href.replace('elettra.trieste.it', 'elettra.eu');
}

if(!app) {document.domain = 'elettra.eu';}

var networkStatus = "";
var fel=[false,false];
var $_GET = getQueryParams(document.location.search);
var base="http://fcsproxy.elettra.eu/docs/fermi/app/";
var baseElettra="http://ecsproxy.elettra.eu/docs/fermi/app/";

if (localStorage.getItem("lastMode")==undefined) {
	localStorage.setItem("lastMode","");
	localStorage.setItem("astorMode","all");
}

function httpGet(url,id,sep,encoded,debug){
	return $.get(url)
		.done(function(data) {
			if (debug==1) console.log(url+' '+id+' '+sep+' ---> ',data);
			if (data.toLowerCase().indexOf("attributo")>-1) {
				$("#"+id).val("PHP parameters error");
			} else if (data.toLowerCase().indexOf("socket")>-1 || data.toLowerCase().indexOf("device")>-1) {
				$("#"+id).val("----");
			} else {
				var x = data.split(sep)[0];
				if (!encoded) {
					if (!isNaN(x)) {
						x=Math.round(1000*x)/1000;
					}
					if (debug==1) console.log($("#"+id).val()+' VS '+$("#"+id).html());
					if (app || $_GET.version=="app" || $_GET.version=="elettraStatus" || $_GET.version=="scwStatus") $("#"+id).val(x); else $("#"+id).html(x);
				}
				else {
					$("#"+id).val((typeof encoded[x] !== 'undefined')? encoded[x]: 'undefined');
				}
			}
		})
		.fail(function() {
			if (app) {
				$("#"+id).val("HTTP error");
			} else {
				$("#"+id).html("HTTP error");
			}
		})
};


$(document).ready( function() {
	if(app) {
		document.addEventListener('deviceready', onDeviceReady);
	} else {
		onDeviceReady();
	}
});

function onDeviceReady() {
	//readwriteTest();
	
	$(window).on('message', function(e) {
		var origin= e.origin || e.originalEvent.origin;
		var data= e.data || e.originalEvent.data;
		if(origin.indexOf("elettra.eu")>-1){
			if (data['eGiga2mOpen']) {
				var locationUrl = 'http://ecsproxy.elettra.eu/docs/egiga2m/egiga2m.html';
				if (data['eGiga2mOpen'].indexOf('fermi')) locationUrl = 'http://fcsproxy.elettra.eu/docs/egiga2m/egiga2m.html';
				if (data['eGiga2mOpen'].indexOf('padres')) locationUrl = 'http://padresproxy.elettra.eu/docs/egiga2m/egiga2m.html';
				document.location = locationUrl+data['eGiga2mOpen'];
			} else if (data["result"]) {
				if (data["result"]!="") {
					$(".menu").hide();
					if (app) {
						$(".loginFrame").hide();
						navigator.notification.confirm(data["result"]+"\n\nDo you wish to retry?",wrongInput,"Action failed",["Retry","Cancel"]);
					} else {
						$(".loginFrameWeb").hide();
						var retry = confirm("Action Failed\n\n"+data["result"]+"\n\nDo you wish to retry?");
						if(retry){
							userHandler({});
						} else {
							alert("Aborting\n\nImpossible to continue with the operation");
						}
					}
				}
			}
		}
	});
	
	if(app) {
		window.cache.clear( cacheClear, cacheNotClear );
		window.cache.cleartemp();
		$(window).on("online",onOnline);
		$(window).on("offline",onOffline);
		
		$("#fermiBtn").on("click", loadFERMI);
		$("#astorBtn").on("click", loadAstor);
		$("#astorPadresBtn").on("click", loadAstorPadres);
		$("#astorElettraBtn").on("click", loadAstorElettra);
		$("#endOfSBtn").on("click", function(e) {loadFullFrame(e);});
		$("#turniOpBtn").on("click", function(e) {loadFullFrame(e);});
		$("#turniPhBtn").on("click", function(e) {loadFullFrame(e);});
		// $("#elettraBtn").on("click", function(e) {loadFullFrame(e);});
		$("#elettraStatus").on("click", function(e) {loadElettraStatus(e);});
		$("#scwStatus").on("click", function(e) {loadScwStatus(e);});
		$("#thresBtn").on("click", loadThresholds);
		loadLastMode();
		
	} else {
		
		if ($_GET.version=="old" || $_GET.version==undefined || $_GET.version=="" ) {
			
			$("head").html(	'<title>FERMI Machine Status</title>'+
							'\n<meta charset="utf-8">'+
							'\n<script src="lib/jquery/dist/jquery.min.js"></script>'+
							'\n<meta name="viewport" content="width=device-width, initial-scale=1">'+
							'\n<link rel="stylesheet" href="http://www/media/system/css/modal.css" type="text/css" />'+
							'\n<link rel="stylesheet" href="http://www/modules/mod_fermi_machine_status/style.css" type="text/css" />'+
							"\n<link rel='stylesheet' href='http://www/templates/elettra/css/layout.css' type='text/css' />"+
							"\n<link rel='stylesheet' href='http://www/templates/elettra/css/template.css' type='text/css' />"+
							"\n<link rel='stylesheet' href='http://www/templates/elettra/css/joomla.css' type='text/css' />"+
							"\n<link rel='stylesheet' href='http://www/templates/elettra/css/classes.css' type='text/css' />"+
							"\n<link rel='stylesheet' href='http://www/templates/elettra/css/plugins.css' type='text/css' />"+
							"\n<link rel='stylesheet' href='http://www/templates/elettra/css/modules.css' type='text/css' />");
			
			$("#mainPage").load(base+"browserIndex.html", function() {
				$(document).ready( function() {
					$.when( aggLente(fel) )
					 .done( function() { aggRapide(fel);})
					 .fail( function () {
						alert("Errore: aggiornamento dei valori principali fallito");
						aggRapide(fel)});
				});
			});
			
		} else if ($_GET.version=="app") {
			$(window).on("resize",iframeRefresh);
			$("#mainPage").load(base+"fermiIndex.html", function() {
				adaptToWidth();
				$(document).ready( function() {
					$.when( aggLente(fel) )
					 .done( function() { aggRapide(fel);})
					 .fail( function () {
						alert("Errore: aggiornamento valori principali fallito");
						aggRapide(fel)});
				});
			});
			
		} else if ($_GET.version=="astor") {
			
			$("#mainPage").load(base+"astorIndex.html", function(){
				$(document).ready( function() {
					getServerTree(localStorage.getItem("astorMode"));
				});
			});
		} else if ($_GET.version=="astorPadres") {			
			$("#mainPage").load(base+"astorPadres.html", function(){
				$(document).ready( function() {
					getServerTree(localStorage.getItem("astorMode"));
				});
			});
		} else if ($_GET.version=="astorElettra") {			
			$("#mainPage").load(baseElettra+"astorElettra.html", function(){
				$(document).ready( function() {
					getServerTree(localStorage.getItem("astorMode"));
				});
			});
		} else if ($_GET.version=="elettraStatus") {
			$(window).on("resize",iframeElettraRefresh);
			$("#mainPage").load("http://ecsproxy.elettra.eu/docs/fermi/app/elettraStatus.html", function() {
				adaptToWidthElettra();
				$(document).ready( function() {
					$.when( aggElettraLente() )
					 .done( function() { aggElettraRapide();})
					 .fail( function () {
						alert("Errore: aggiornamento valori principali fallito");
						aggElettraRapide(fel)});
				});
			});
		} else if ($_GET.version=="scwStatus") {
			$(window).on("resize",iframeRefresh);
			$("#mainPage").load("http://ecsproxy.elettra.eu/docs/fermi/app/scwStatus.html", function() {
				adaptToWidthScw();
				$(document).ready( function() {
					$.when( aggScwLente() )
					 .done( function() { aggScwRapide();})
					 .fail( function () {
						alert("Errore: aggiornamento valori principali fallito");
						aggScwRapide(fel)});
				});
			});
		}
		else {
			loadFullFrame($_GET.version);
		}
	}
};
/*
function writeToFile(fileName,data) {
	data = JSON.stringify(data, null, '\t');
	console.log(data);
	window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (directoryEntry) {
		
		directoryEntry.getFile(fileName, { create: true }, function (fileEntry) {
			console.log(fileEntry);
			fileEntry.createWriter(function (fileWriter) {
				
				fileWriter.onwriteend = function (e) {
					console.log('Write of file "' + fileName + '"" completed.');
				};
				
				fileWriter.onerror = function (e) {
					console.log('Write failed: ' + e.toString());
				};

				var blob = new Blob([data], { type: 'text/plain' });
				fileWriter.write(blob);
			}, function(e){
			console.log(e.toString());});
		}, null);
	}, null);
}

function readFromFile(fileName, callback) {
	var pathToFile = cordova.file.dataDirectory + fileName;
	window.resolveLocalFileSystemURL(pathToFile, function (fileEntry) {
		
		fileEntry.file(function (file) {
			var reader = new FileReader();

			reader.onloadend = function (e) {
				callback(JSON.parse(this.result));
			};

			reader.readAsText(file);
		}, null);
	}, null);
}

function readwriteTest() {
	writeToFile('modes.json', { lastMode: "ciccio" });
	var fileData;
    readFromFile('modes.json', function (data) {
        fileData = data;
    });
	console.log(fileData);
}
*/
function loadLastMode(){
	switch (localStorage.getItem("lastMode")) {
			case "Astor":
				loadAstor();
				break;
			case "AstorPadres":
				loadAstor();
				break;
			case "AstorElettra":
				loadAstorElettra();
				break;
			case "FERMI Status":
				loadFERMI();
				break;
			case "End of Shift":
				loadFullFrame("End of Shift");
				break;
			case "Turni Operatori":
				loadFullFrame("Turni Operatori");
				break;
			case "Turni Fisici":
				loadFullFrame("Turni Fisici");
				break;
			case "Elettra Status":
				loadFullFrame("Elettra Status");
				break;
		}
}

function loadFullFrame(e) {
	var title,source,btnId;
	if (typeof e!="string") {
		btnId=$(e.target).prop("id");
	} else {
		btnId=e;
	}
	switch (btnId) {
		case "elettraBtn":
			title="Elettra Status";
			source="http://elog.elettra.eu/informationsystem/web.asp";
			break;
		case "endOfSBtn":
			title="End of Shift FERMI";
			source="http://felog.elettra.eu/forum.asp?FORUM_ID=66";
			break;
		case "turniOpBtn":
			title="Turni Operatori";
			source="http://fcsproxy.elettra.eu/docs/fermi/app/turni_operatori.php";
			break;
		case "turniPhBtn":
			title="Turni Fisici FERMI";
			source="http://fcsproxy.elettra.eu/docs/fermi/app/turni_fisici.php";
			break;
	}
	if(app) {
		remember = document.forms["rememberMode"]["checkbox"].checked;
		if(remember){
			aggLastMode(title);
		} else {
			localStorage.setItem("lastMode","");
		}
	}
	
	$("#mainPage").load("fullFrameIndex.html", function(responseText,textStatus,jqXHR) {
		if(textStatus=="error") {
			$("#mainPage").html('<div class="media bg-info"> <div class="media-left media-middle"> <img src="img/icon.png" class="media-object" style="width:40px" /> </div> <div class="media-body media-middle"> <h4 class="text-center center-block media-heading" >Astor</h4></div></div><hr><div id="networkErr" class="container-fluid" style="display:none"><h4 class="text-danger text-center">Error: not possible to load the page</h4></div>');
		} else {
			$(document).ready( function() {
				if (app) {$(".media-left").html('<a onclick="localStorage.setItem(\'lastMode\',\'\')" href="index.html"> <img src="img/icon.png" class="media-object" style="width:40px" /></a>');}
				$("#pageTitle").html(title);
				$(".fullFrame").css("padding-bottom",$(window).height()-55);
				$("#fullframe").prop("src",source);
			});
		}
	});	
}

function aggLastMode(title) {
	localStorage.setItem("lastMode",title);
	navigator.notification.alert("Tap on Elettra-Sincrotrone logo to return to the mode selection page",null,"Default Mode: "+title);
}

function getQueryParams(qs) {
		qs = qs.split("+").join(" ");
		var params = {},
						tokens,
						re = /[?&]?([^=]+)=([^&]*)/g;
		while (tokens = re.exec(qs)) {
				params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
		}
		return params;
};

function onOnline() {
	if(networkStatus != "connected" && networkStatus != "") {
		networkStatus="connected";
		$("#networkErr").hide();
		navigator.notification.alert("Connessione alla rete ristabilita", null, "Online");
		$("#main").show();
	}
};

function onOffline() {
	if(networkStatus != "disconnected") {
		networkStatus="disconnected";
		$("#main").hide();
		navigator.notification.alert("Impossibile visualizzare i dati in modalità offline", null, "Offline");
		$("#networkErr").show();
	}
};

function cacheClear (status) {
	//alert('Message: ' + status);
};

function cacheNotClear (status) {
	navigator.notification.alert("Errore: "+status+"\nLa cache non è stata pulita correttamente, i grafici potrebbero essere datati.\nRiavviare l'applicazione", null, "Errore");
};
