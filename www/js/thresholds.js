if (localStorage.getItem("thresholds")==undefined) {
	localStorage.setItem("thresholds",JSON.stringify({}));
}

function loadThresholds() {
	$("#mainPage").load("thresholdsIndex.html", function(responseText,textStatus,jqXHR) {
		if(textStatus=="error") {
			$("#mainPage").html('<div class="media bg-info"> <div class="media-left media-middle"> <img src="img/icon.png" class="media-object" style="width:40px" /> </div> <div class="media-body media-middle"> <h4 class="text-center center-block media-heading" >Astor</h4></div></div><hr><div id="networkErr" class="container-fluid" style="display:none"><h4 class="text-danger text-center">Error: not possible to load the page</h4></div>');
		} else {
			$(document).ready( function() {
				if (app) {$(".media-left").html('<a onclick="localStorage.setItem(\'lastMode\',\'\')" href="index.html"> <img src="img/icon.png" class="media-object" style="width:40px" /></a>');}
				buildThresList();
			});
		}
	});
}

function buildThresList() {
	var list=JSON.parse(localStorage.getItem("thresholds"));
	$("#thresList").empty();
	if (list!=undefined){
		for (var attr in list) {
			if (list[attr].active) {
				$("#thresList").append('<li id="'+attr+'" onclick="openMenu(\''+attr+'\')"style="cursor:pointer" class="list-group-item">'+attr+" "+list[attr].opt+" "+list[attr].value+'</li>');
			}
		}
		thresholdsAction();
	}
}

function thresholdsAction() {
	var attr = $("#menuTitle").html();
	$("#delThres").on("click",function(e) {
		if(app) {
			navigator.notification.confirm("Are you sure you want to delete "+attr+" threshold?",function(res) {deleteThres(res,attr);},"Deleting Threshold",["Yes","No"]);
		} else {
			var res = confirm("Are you sure you want to delete "+attr+" threshold?");
			deleteThres(res);
		}
	});
	$("#modThres").on("click",function() {askThreshold(attr)});
	$("#modNot").on("click",function() {modifyNotification(attr)});
}

function modifyNotification(attribute) {
	var thresList=JSON.parse(localStorage.getItem("thresholds"));
	var threshold=thresList[attribute];
	if (app) {
		navigator.notification.prompt(	"Type a number\n(0 to prevent beeping\nAny decimal number will be rounded)",
										function(res) {
											if(res.buttonIndex==1){
												if (typeof res.input1=="number"){
													threshold.nBeep=Math.round(res.input1);
													localStorage.setItem("thresholds",JSON.stringify(thresList));
												} else {
													navigator.notification.alert("Please type in a number\n(Any decimal number will be rounded)",function() {modifyNotification(attribute);},"Invalid input!");
												}
											}
										},
										"Beeps number",
										["Submit","Skip"]);
		navigator.notification.prompt(	"Type the number of ms\n(0 to prevent vibrating)",
										function(res) {
											if(res.buttonIndex==1){
												if(typeof res.input1=="number"){
													threshold.duration=Math.round(res.input1);
													localStorage.setItem("thresholds",JSON.stringify(thresList));
												} else {
													navigator.notification.alert("Please type in a number\n(Any decimal number will be rounded)",function() {modifyNotification(attribute);},"Invalid input!");
												}
											}
										},
										"Vibration duration",
										["Submit","Skip"]);
	} else {
		var numBeep = prompt("Beeps number\n\nType an integer number\n(0 to prevent beeping)","");
		if (numBeep!=null) {
			if(typeof numBeep=="number"){
				threshold.nBeep=Math.round(numBeep);
				localStorage.setItem("thresholds",JSON.stringify(thresList));
			} else {
				alert("Invalid input!\n\nPlease type in a number\n(Any decimal number will be rounded)",function() {modifyNotification(attribute);});
			}
		}
		var vibrDur = prompt("Vibration duration\n\nType the number of ms\n(0 to prevent vibrating)","");
		if (vibrDur){
			if(typeof vibrDur=="number"){
				threshold.duration=Math.round(vibrDur);
				localStorage.setItem("thresholds",JSON.stringify(thresList));
			} else {
				alert("Invalid input!\n\nPlease type in a number\n(Any decimal number will be rounded)",function() {modifyNotification(attribute);});
			}
		}
	}
}

function deleteThres(btnIndex,attribute) {
	if (btnIndex==1) {
		
		$('.menu').hide('fast');
		var thresList = JSON.parse(localStorage.getItem("thresholds"));
		thresList[attribute].active=false;
		localStorage.setItem("thresholds",JSON.stringify(thresList));
		
		clearTimeout(thresList[attribute].timeout);
		buildThresList();
		navigator.notification.alert("",null,"Threshold deleted");
	}
}

function showAttrList() {

	var deviceServer=$("#menuTitle").html().split("//")[1];
	// read device of server (any class)
	$.get("http://fcsproxy.elettra.eu/docs/tangoproxy/fermistatus.php?readdevicename="+deviceServer+",*")
		.done(function (data) {
			$(".menu>ul").empty();
			var device_data = data.split(',');
			for (var j=0; j<device_data.length; j++) {
				if (!device_data[j].includes('dserver/')){
					// read list of attributes and commands ?readattributelist=device_data[j]
					$.get("http://fcsproxy.elettra.eu/docs/tangoproxy/fermistatus.php?DeviceProxy="+device_data[j]+",get_attribute_list")
						.done(function (list_data) {
							if (list_data.includes("socket")){
								$(".menu>ul").append('<a class="list-group-item" href="#">fermistatus not responding</a>'+
														'<a class="list-group-item" href="#">Impossible to retrieve data</a>');
							} else {
								var list = list_data.split(',');
								var deviceData=this.url.split("DeviceProxy=")[1].split(",get_attribute_list")[0];
								//ottiene la lista degli attributi
								for (var i=0;i<list.length;i++) {
									list[i]=deviceData+"/"+list[i];
									$(".menu>ul").append('<a id="'+list[i]+'" class="list-group-item" href="#">'+list[i]+'</a>');
									$("#"+list[i].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")).on("click", askThreshold);
								}
							}
						})
				}
			}
		})
}

function initThreshold(attr,operator,val) {
	var thresList=JSON.parse(localStorage.getItem("thresholds"));
	thresList[attr]={	opt:operator,
						value:val,
						justNot:false,
						nBeep:1,
						duration:500};
	localStorage.setItem("thresholds",JSON.stringify(thresList));
}

function askThreshold(e){
	if (typeof e=="string") {
		var attr=e;
	} else {
		var attr=$(e.target).prop("id");
	}
	if(app) {
		navigator.notification.prompt("Operators available:\n'>' '<'\n(Please use . as decimal separator)",
										function(results) {
											if (results.buttonIndex==1) {
												var opt = results.input1.trim().substring(0,1);
												var value;
												
												if (isNaN(Number(results.input1.trim().substring(1,2)))) {
													opt+=results.input1.trim().substring(1,2);
													if (typeof Number(results.input1.trim().substring(2).trim())=="number") {
														value = Number(results.input1.trim().substring(2).trim());
													} else {
														navigator.notification.alert("Please type in a number\nUse . as decimal separator",function() {askThreshold(e);},"Invalid threshold!");
													}
												} else {
													if (typeof Number(results.input1.trim().substring(1).trim())=="number") {
														value = Number(results.input1.trim().substring(1).trim());
													} else {
														navigator.notification.alert("Please type in a number\nUse . as decimal separator",function() {askThreshold(e);},"Invalid threshold!");
													}
												}
												
												if (opt!="<" && opt !=">") {
													navigator.notification.alert("Please use only < or >",function() {askThreshold(e);},"Invalid operator!");
												}
												
												$('.menu').hide('fast');
												initThreshold(attr,opt,value);
												setThreshold(attr);
											}
										},
										attr+" threshold",
										["Confirm","Cancel"]);
	} else {
		var res = prompt(attr+" threshold\n\nOperators available:\n'>' '<'\n(Please use . as decimal separator)","");
		if (res!=null) {
			var opt = res.trim().substring(0,1);
			var value;
			
			if (isNaN(Number(res.trim().substring(1,2)))) {
				opt+=res.trim().substring(1,2);
				if (typeof Number(res.trim().substring(2).trim())=="number") {
					value = Number(res.trim().substring(2).trim());
				} else {
					navigator.notification.alert("Please type in a number\nUse . as decimal separator",function() {askThreshold(e);},"Invalid threshold!");
				}
			} else {
				if (typeof Number(res.trim().substring(1).trim())=="number") {
					value = Number(res.trim().substring(1).trim());
				} else {
					navigator.notification.alert("Please type in a number\nUse . as decimal separator",function() {askThreshold(e);},"Invalid threshold!");
				}
			}
			
			if (opt!="<" && opt !=">") {
				navigator.notification.alert("Please use only < or >",function() {askThreshold(e);},"Invalid operator!");
			}
			
			$('.menu').hide('fast');
			initThreshold(attr,opt,value);
			setThreshold(attr);
		}
	}
}

function setThreshold(attribute){
	var actualValue;
	var thresList = JSON.parse(localStorage.getItem("thresholds"));
	var threshold = thresList[attribute];
	
	//ottiene valore di attributo a seconda dell'id di e.target
	$.get("http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var="+attribute)
	.done(function (data) {
		
		actualValue=Number(data.split(":")[0]);
		
		if(app){
			switch (threshold.opt) {
				case ">":
					if(actualValue>threshold.value){
						if(!threshold.justNot) {
							navigator.notification.beep(threshold.nBeep);
							navigator.vibrate(threshold.duration);
						}
						threshold.justNot=true;
					} else {
						threshold.justNot=false;
					}
					break;
				case "<":
					if(actualValue<threshold.value){
						if(!threshold.justNot) {
							navigator.notification.beep(threshold.nBeep);
							navigator.vibrate(threshold.duration);
						}
						threshold.justNot=true;
					} else {
						threshold.justNot=false;
					}
					break;
			}
		} else {
			switch (threshold.opt) {
				case ">":
					if(actualValue>threshold.value){
						if(!threshold.justNot) {
							for(var i=0;i<threshold.nBeep;i++){ beep(); }
						}
						threshold.justNot=true;
					} else {
						threshold.justNot=false;
					}
					break;
				case "<":
					if(actualValue<threshold.value){
						if(!threshold.justNot) {
							for(var i=0;i<threshold.nBeep;i++){ beep(); }
						}
						threshold.justNot=true;
					} else {
						threshold.justNot=false;
					}
					break;
			}
		}
		threshold.timeout = setTimeout(function(){setThreshold(attribute);},3000);
		threshold.active=true;
		thresList[attribute]=threshold;
		localStorage.setItem("thresholds",JSON.stringify(thresList));
	});
}
function beep() {
    var snd = new  Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");  
    snd.play();
}