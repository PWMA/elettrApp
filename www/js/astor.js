var globalTimeout;
var astorSrvTimeout;
var alertTimeout;
var devsrvTimeouts={};
var ev;

var starterMonitor = "http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var=f/modulators/sbandmonitor/GlobalState";
var starter = "http://fcsproxy.elettra.eu/docs/tangoproxy/fermi.php?var";
var baseHtml = "astorIndex.html";

if ($_GET.version=="astorElettra") {
	starterMonitor = "http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=tango/admin/startermonitor/GlobalState";
	starter = "http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var";
	baseHtml = "astorElettra.html";
}
else if ($_GET.version=="astorPadres") {
	starterMonitor = "http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=f/misc/startermonitor/GlobalState";
	starter = "http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var";
	baseHtml = "astorPadres.html";
}


function loadAstorElettra(){
	starterMonitor = "http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var=tango/admin/startermonitor/GlobalState";
	starter = "http://ecsproxy.elettra.eu/docs/tangoproxy/index.php?var";
	baseHtml = "astorElettra.html";
	console.log('loadAstorElettra(), baseHtml: '+baseHtml);
	loadAstor();
}

function loadAstorPadres(){
	starterMonitor = "http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var=f/misc/startermonitor/GlobalState";
	starter = "http://fcsproxy.elettra.eu/docs/tangoproxy/padres.php?var";
	baseHtml = "astorPadres.html";
	loadAstor();
}

function loadAstor(){
	var title="Astor";
	console.log('baseHtml: '+baseHtml);
	if(app) {
		if (document.forms["rememberMode"]!=undefined) {
			remember = document.forms["rememberMode"]["checkbox"].checked;
			if(remember){
				aggLastMode(title);
			} else {
				localStorage.setItem("lastMode","");
			}
		}
	}
	$("#mainPage").load(baseHtml, function(responseText,textStatus,jqXHR) {
		if(textStatus=="error") {
			$("#mainPage").html('<div class="media bg-info"> <div class="media-left media-middle"> <img src="img/icon.png" class="media-object" style="width:40px" /> </div> <div class="media-body media-middle"> <h4 class="text-center center-block media-heading" >Astor</h4></div></div><hr><div id="networkErr" class="container-fluid" style="display:none"><h4 class="text-danger text-center">Error: not possible to load the page</h4></div>');
		} else {
			$(document).ready( function() {
				if (app) {$(".media-left").html('<a onclick="localStorage.setItem(\'lastMode\',\'\')" href="index.html"> <img src="img/icon.png" class="media-object" style="width:40px" /></a>');}
				getServerTree(localStorage.getItem("astorMode"));
			});
		}
	});
}

function actionControl() {
	$("#startStop").on("click",function(e) {
		ev=e;
		$('.menu').hide('fast');
		if(app) {
			if(localStorage.getItem("username")==undefined) {
				navigator.notification.prompt("Action: "+$(e.target).html()+"\n\nPlease enter your username(only once):",userHandler,"Login required",["Submit","Cancel"]);
			} else {
				userHandler({buttonIndex:1,input1:localStorage.getItem("username")});
			}
		} else {
			userHandler({});
		}
	});
	$("#attrList").on("click",showAttrList);
}

function userHandler(results){
	if (app) {
		if(results.buttonIndex==1) {
			var deviceServer=$("#menuTitle").html().split("//")[1];
			var srv = $("#menuTitle").html().split("//")[0];
			var action=$(ev.target).html().split(" ")[0];
			var token="app:";
			
			localStorage.setItem("username",results.input1);
			token+="tango/admin/"+srv+"->"+"Dev"+action+","+deviceServer;
			localStorage.setItem("token",token);
			$("#loginframe").prop("src","https://www.elettra.eu/service/pss/pss_login.php?pss_username="+results.input1+"&pss_token="+token);
			$(".loginFrame").show();
		}
	} else {
		var username = (localStorage.getItem("username")==undefined)? '': localStorage.getItem("username");
		var user = prompt("Action: "+$(ev.target).html()+"\n\nPlease enter your username:", username);
		if (user!=null){
			var deviceServer=$("#menuTitle").html().split("//")[1];
			var srv=$("#menuTitle").html().split("//")[0];
			var action=$(ev.target).html().split(" ")[0];
			var token="app:";
			
			localStorage.setItem("username",user);
			token+="tango/admin/"+srv+"->"+"Dev"+action+","+deviceServer;
			localStorage.setItem("token",token);
			$(".loginFrameWeb").css("left",$(window).width()/3);
			$(".loginFrameWeb").css("top",$(window).scrollTop()+$(window).height()/10);
			$(".loginFrameWeb").css("width",200);
			$(".loginFrameWeb").css("height",350);
			$("#loginframe").prop("src","https://www.elettra.eu/service/pss/pss_login.php?pss_username="+user+"&pss_token="+token);
			$(".loginFrameWeb").show();
		}
	}
}

function wrongInput(buttonIndex) {
	if (buttonIndex==1) {
		navigator.notification.prompt("Action: "+$(ev.target).html()+"\n\nPlease enter your username(only once):",userHandler,"Login required",["Submit","Cancel"]);
	} else {
		navigator.notification.alert("Impossible to continue with the operation",null,"Aborting");
	}
}

function openMenu(e){
	
	var name = $(e.target).parent().prop("id");
	$('#menuTitle').html(name);
	if(name.split("//")[1]!=undefined){
		var server=name.split("//")[0];
		name=name.split("//")[1];
	}
	$(".menu>ul").empty();
	var page = $("#pageTitle").html();
console.log('openMenu: '+name+', page: '+page);
	if(app) {
		if (page.indexOf("Server")>-1 || page.indexOf("alert")>-1) {
			$(".menu>ul").append('<a id="startStop" class="list-group-item" href="#">Start Server</a>'
								//+'<a id="attrList" class="list-group-item" href="#">Show attributes list</a>'
								);
			actionControl();
			if($("#"+server+"\\/\\/"+name.replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")+">span>img").prop("src").indexOf("red")>-1) {
				$('#startStop').html("Start Server");
			} else {
				$('#startStop').html("Stop Server");
			}
		} else if (page.indexOf("Alarms")>-1) {
			$(".menu>ul").append('<a id="delThres" class="list-group-item" href="#">Delete Threshold</a>'
								+'<a id="modThres" class="list-group-item" href="#">Modify Threshold</a>'
								+'<a id="modNot" class="list-group-item" href="#">Modify Notifications</a>');
			thresholdsAction();
		} else if (page.indexOf("Astor")>-1) {
			$(".menu>ul").append('<li id="openCtrlP" class="list-group-item">Open Control Panel</li>');
			if (!$("#"+name+">span>img").prop("src").indexOf("red")>-1) {
				$("#openCtrlP").on("click", function() {clearTimeout(globalTimeout);loadDeviceServerOf($('#menuTitle').html());});
			}
		}
	} else {
		if (page.indexOf("Alarms")>-1) {
			$(".menu>ul").append('<a id="delThres" class="list-group-item" href="#">Delete Threshold</a>'
								+'<a id="modThres" class="list-group-item" href="#">Modify Threshold</a>'
								+'<a id="modNot" class="list-group-item" href="#">Modify Notifications</a>');
			thresholdsAction();
		} else if (page.indexOf("Astor")>-1|| page.indexOf("alert")>-1){
			$(".menu>ul").append('<a id="startStop" class="list-group-item" href="#">Start Server</a>'
								// +'<a id="attrList" class="list-group-item" href="#">Show attributes list</a>'
								);
			actionControl();
			if($("#"+server+"\\/\\/"+name.replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")+">span>img").prop("src").indexOf("red")>-1) {
				$('#startStop').html("Start Server");
			} else {
				$('#startStop').html("Stop Server");
			}
		}
	}
	
	$(".menu").css("top",$(window).scrollTop()+$(window).height()/10);
	$('.menu').show("fast");
}

function changeMode() {
	var mode = localStorage.getItem("astorMode");
	if (mode=="all"){
		clearTimeout(globalTimeout);
		$.each(devsrvTimeouts, function(property,value){
			clearTimeout(value);
		});
		localStorage.setItem("astorMode","alert");
	} else if (mode=="alert") {
		clearTimeout(alertTimeout);
/* // thresholds mode inhibited on 01/02/2017
		localStorage.setItem("astorMode","thresholds");
	} else if (mode=="thresholds"){
*/
		console.log('localStorage.setItem()');
		localStorage.setItem("astorMode","all");
	}
console.log('changeMode(): '+mode);
	loadAstor();
}

function updateStatusOf(id) {
	if (!app && id.indexOf("level")>-1) {
		id=id.split("/level")[0]+">ul>li:eq("+id.split("level")[1]+")";
	}
	var finalColor="green";
	$("#"+id+">ul>li").each(function() {
		var color = $(this).find(">span>img").prop("src").split("img/leds/images48/led")[1].split(".png")[0];
		if(color!=finalColor) {
			switch(finalColor){
				case "green":
					finalColor=color;
					break;
				case "blue":
					if(color!="green"){finalColor=color;}
					break;
				case "orange":
					if(color=="red"){finalColor=color;}
				case "red":
					break;
			}
		}
	});
	$("#"+id+">span>img").prop("src","img/leds/images48/led"+finalColor+".png");
}

function getServerTree(mode) {
	if (mode=="all"){
		
		$(".tree").show();
		
		$.get(starterMonitor)
			.done(function (data) {
				var rows = data.split("\n");
				for(var i=0;rows[i].indexOf(":")==-1;i++) {
					var color="red";
					
					var row = rows[i];
					var server= row.split(",")[0].trim();
					
					var sstatus = row.split(",")[1];
					var fulltitle = server;
					if (row.split(",")[2]!=undefined) { var parent_name = row.split(",")[2].replace(/\s/g,""); }
					
					if(row.split(",")[3]!=undefined && row.split(",")[3]!="") {fulltitle+=" ("+row.split(",")[3]+")";}
					
					switch (sstatus) {
						case "0":
							color="green";
							break;
						case "11":
							color="orange";
							break;
						case "6":
							color="blue";
							break;
						default:
							color="red";
							break;
					}
					
					if ($("#"+server).length==0) {
						// console.log('server: '+server+', sstatus: '+sstatus+', row.split(",")[2]: '+row.split(",")[2]+', parent_name: '+parent_name);
						$("#"+parent_name+">ul").append('<li id="'+server+'" style="display:none;cursor:pointer"><span><img src="img/leds/images48/led'+color+'.png" width="17px" height="17px"/>&nbsp;'+fulltitle+'</span> </li>');
						if(app) {
							$("#"+server).on("click", openMenu);
						} else {
							$("#"+server).on("click", getDevSrvWeb);
						}
					} else {
						var actualSrvState = $("#"+server+">span>img").prop("src");
						if (actualSrvState.indexOf(color)==-1){
							$("#"+server+">span>img").prop("src","img/leds/images48/led"+color+".png");
						}
					}					
					updateStatusOf(parent_name);					
					if(!app) {
						$(".menu>.list-group").html('<a id="startStop" class="list-group-item"href="#">Start Server</a>'
													//+'<a id="attrList" class="list-group-item" href="#">Show attributes list</a>'
													);
						actionControl();
					}
				}
				$('.tree li:has(ul)').prop("class",'parent_li');
				$('.tree li.parent_li > span').off('click', showChildren);
				$('.tree li.parent_li > span').on('click', showChildren);
				
				globalTimeout = setTimeout(function(){getServerTree("all");},120000);
		})
	} else if (mode=="alert") {
		
		$("#mainPage").load("astorServerIndex.html", function() {
			
			actionControl();
			$("#pageTitle").html("Astor alert mode");
			$(".media").css("height","40px");
			if(app) {$("#backBtn").html('<a style="cursor:pointer" onclick="localStorage.setItem(\'lastMode\',\'\')" href="index.html"> <img src="img/icon.png" class="media-object" style="width:40px" /></a>');
			} else {$("#backBtn").html('<img src="img/icon.png" class="media-object" style="width:40px" />');}
			
			$(".tree").hide();
			$("#alertList").show();
			getAlertList();
		});
	} else if (mode=="thresholds") {
		loadThresholds();
	}
};

function getDevSrvWeb(e) {
	var server = $(e.target).parent().prop("id");
	clearTimeout(devsrvTimeouts[server]);
	
	$("#"+server).off("click", getDevSrvWeb);
	$("#"+server+">span").off("click");
	$("#"+server+">span").on("click", function(eve){
		var server2=$(eve.target).parent().prop("id");
		clearTimeout(devsrvTimeouts[server2]);
		$("#"+server2+">span").off("click");
		$("#"+server2).on("click", getDevSrvWeb);
	});
	
	var actualSrvState = $("#"+server+">span>img").prop("src");
	if (actualSrvState.indexOf("red")==-1){
		
		if ($("#"+server+":not(:has(ul))").length!=0) {
			$("#"+server).append('<ul>'
									+'<li style="display:none"><span ><img src="img/leds/images48/ledgreen.png" height="17px" width="17px"/>&nbsp;Not controlled</span><ul></ul></li>'
									+'<li style="display:none"><span ><img src="img/leds/images48/ledgreen.png" height="17px" width="17px"/>&nbsp;Level 1</span><ul></ul></li>'
									+'<li style="display:none"><span ><img src="img/leds/images48/ledgreen.png" height="17px" width="17px"/>&nbsp;Level 2</span><ul></ul></li>'
									+'<li style="display:none"><span ><img src="img/leds/images48/ledgreen.png" height="17px" width="17px"/>&nbsp;Level 3</span><ul></ul></li>'
									+'<li style="display:none"><span ><img src="img/leds/images48/ledgreen.png" height="17px" width="17px"/>&nbsp;Level 4</span><ul></ul></li>'
									+'<li style="display:none"><span ><img src="img/leds/images48/ledgreen.png" height="17px" width="17px"/>&nbsp;Level 5</span><ul></ul></li>'
								+'</ul>');
		}
		
		
		$.when($.get(starter+"=tango/admin/"+server+"/Servers")
			.done(function (data2) {
				var notSepList=data2.split("(")[1];
				if (notSepList!=undefined){
					notSepList=notSepList.split(")")[0];
					var list=[];
					var deviceServerName=[];
					var sstatus2=[];
					var level=[];
					var sserver=this.url.split("admin/")[1].split("/Servers")[0];
					
					for(var j=0;notSepList.split(", ")[j]!=undefined;j++) {
						var el = notSepList.split(",")[j];
						el = el.split("'")[1].split("'")[0];
						el = el.trim();
						list.push(el);
						
						if (list[j].split("\t").length>1) {
							deviceServerName.push(list[j].split("\t")[0]);
							sstatus2.push(list[j].split("\t")[1]);
							level.push(list[j].split("\t")[3]);
						} else {
							deviceServerName.push(list[j].split("\\t")[0]);
							sstatus2.push(list[j].split("\\t")[1]);
							level.push(list[j].split("\\t")[3]);
						}
					}
					
					for(var j=0;j<list.length;j++) {
						var color2;
						
						switch (sstatus2[j]) {
							case "ON":
								color2="green";
								break;
							case "ALARM":
								color2="orange";
								break;
							case "MOVING":
								color2="blue";
								break;
							default:
								color2="red";
								break;
						}
						var switchButton = '&nbsp;<!--img id=\''+sserver+"//"+deviceServerName[j]+'\' src="img/power_'+(color2=='red'? 'on': 'off')+'.png" width="17px" height="17px"/-->'
						if($("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")).length==0){
							$("#"+sserver+">ul>li:eq("+level[j]+")>ul").append('<li id=\''+sserver+"//"+deviceServerName[j]+'\' style="cursor:pointer;display:none"> <span><img src="img/leds/images48/led'+color2+'.png" width="17px" height="17px"/>&nbsp;'+deviceServerName[j]+switchButton+'</span></li>');
							$("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")).on("click",openMenu);
						} else {
							var actualDevSrvState = $("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")+">span>img").prop("src");
							if (actualDevSrvState.indexOf(color2)==-1){
								$("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")+">span>img").prop("src","img/leds/images48/led"+color2+".png");
							}
						}
						
						updateStatusOf(sserver+"/level"+level[j]);
					}
					$("#"+sserver+">ul>li>ul:empty").parent().remove();
					
					$('.tree li:has(ul)').prop("class",'parent_li');
					$('.tree li.parent_li > span').off('click', showChildren);
					$('.tree li.parent_li > span').on('click', showChildren);
				}
				$(e.target).parent().find(">ul>li").show("fast");
			})).done(function(){
				devsrvTimeouts[server] = setTimeout(function(){ getDevSrvWeb(e);},5000);
			})
	}
}

function showChildren(e) {
	var children = $(this).parent('li.parent_li').find(' > ul > li');
	if (children.is(":visible")) {
		children.hide('fast');
	} else {
		children.show('fast');
	}
	e.stopPropagation();
}

function loadDeviceServerOf(name) {
	$("#mainPage").load("astorServerIndex.html", function() {
		
		actionControl();
		$("#pageTitle").html("Server: "+name);
		$("#backBtn").on("click", function() {clearTimeout(astorSrvTimeout);loadAstor();});
		$(".media-right").hide();
		$("#alertList").hide();
		$(".tree").show();
		
		getDevSrvApp(name);
	})
};

function getDevSrvApp(name){
	$.when($.get(starter+"=tango/admin/"+name+"/Servers")
		.done(function (data) {
			var notSepList=data.split("(")[1].split(")")[0];
			var list=[];
			var deviceServerName=[];
			var sstatus=[];
			var level=[];
			
			for(var i=0;notSepList.split(", ")[i]!=undefined;i++) {
				var el = notSepList.split(",")[i];
				el = el.split("'")[1].split("'")[0];
				el = el.trim();
				list.push(el);
				
				if (list[i].split("\t").length>1) {
						deviceServerName.push(list[i].split("\t")[0]);
						sstatus.push(list[i].split("\t")[1]);
						level.push(list[i].split("\t")[3]);
				} else {
					deviceServerName.push(list[i].split("\\t")[0]);
					sstatus.push(list[i].split("\\t")[1]);
					level.push(list[i].split("\\t")[3]);
				}
			}
			
			for(var i=0;i<list.length;i++) {
				var color;
				
				switch (sstatus[i]) {
					case "ON":
						color="green";
						break;
					case "ALARM":
						color="orange";
						break;
					case "MOVING":
						color="blue";
						break;
					default:
						color="red";
						break;
				}
				
				$("#level"+level[i]).show();
				
				if($("#"+name+"\\/\\/"+deviceServerName[i].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")).length==0){
					$("#level"+level[i]+">ul").append('<li id=\''+name+"//"+deviceServerName[i]+'\' style="cursor:pointer;display:none"> <span><img src="img/leds/images48/led'+color+'.png" width="17px" height="17px"/>&nbsp;'+deviceServerName[i]+'</span> </li>');
					$("#"+name+"\\/\\/"+deviceServerName[i].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")).on("click",openMenu);
				} else {
					var actualDevSrvState = $("#"+name+"\\/\\/"+deviceServerName[i].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")+">span>img").prop("src");
					if (actualDevSrvState.indexOf(color)==-1){
						$("#"+name+"\\/\\/"+deviceServerName[i].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")+">span>img").prop("src","img/leds/images48/led"+color+".png");
					}
				}
				
				updateStatusOf("level"+level[i]);
			}
			$(".tree>ul>li>ul>li>ul:not(:has(*))").parent().remove();
			
			$('.tree li:has(ul)').prop("class",'parent_li');
				$('.tree li.parent_li > span').off('click', showChildren);
				$('.tree li.parent_li > span').on('click', showChildren);
				
		})).done(function(){
			astorSrvTimeout = setTimeout(function(){getDevSrvApp(name);},5000);
		})
}
									   
function getAlertList() {
	
	$.get(starterMonitor)
	.done(function (data) {
		for(var i=0;data.split("\n")[i].indexOf(":")==-1;i++) {
			var color="red";
			var row = data.split("\n")[i];
			var server = row.split(",")[0].trim();
			var sstatus = row.split(",")[1];
			
			switch (sstatus) {
				case "0":
					color="green";
					break;
				case "11":
					color="orange";
					break;
				case "6":
					color="blue";
					break;
				default:
					color="red";
					break;
			}
			if(color=="orange") {
				$.get(starter+"=tango/admin/"+server+"/Servers")
					.done(function (data2) {
						var notSepList=data2.split("(")[1].split(")")[0];
						var list=[];
						var deviceServerName=[];
						var sstatus2=[];
						var sserver=this.url.split("admin/")[1].split("/Servers")[0];
						
						for(var j=0;notSepList.split(", ")[j]!=undefined;j++) {
							var el = notSepList.split(",")[j];
							el = el.split("'")[1].split("'")[0];
							el = el.trim();
							list.push(el);
							
							if(list[j].split("\\t").length>1){
								deviceServerName.push(list[j].split("\\t")[0]);
								sstatus2.push(list[j].split("\\t")[1]);
							} else {
								deviceServerName.push(list[j].split("\t")[0]);
								sstatus2.push(list[j].split("\t")[1]);
							}
						}
						
						for(var j=0;j<list.length;j++) {
							var color2;
							
							switch (sstatus2[j]) {
								case "ON":
									color2="green";
									break;
								case "ALARM":
									color2="orange";
									break;
								case "MOVING":
									color2="blue";
									break;
								default:
									color2="red";
									break;
							}
							
						
							if($("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")).length==0){
								if(color2!="green" && color2!="blue") {
									$("#alertList").append('<li id="'+sserver+"//"+deviceServerName[j]+'" style="cursor:pointer" class="list-group-item"><span><img src="img/leds/images48/led'+color2+'.png" width="17px" height="17px"/>&nbsp;'+sserver+"//"+deviceServerName[j]+'</span></li>');
									$("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")).on("click", openMenu);
								}
							} else {
								if(color2!="green" && color2!="blue") {
									var actualDevSrvState = $("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")+">span>img").prop("src");
									if (actualDevSrvState.indexOf(color2)==-1){
										$("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")+">span>img").prop("src","img/leds/images48/led"+color2+".png");
									}
								} else {
									$("#"+sserver+"\\/\\/"+deviceServerName[j].replace(/\//g,"\\/").replace(/\-/g,"\\-").replace(/\./g,"\\.")).remove();
								}
							}
						}
					})
			}
		}
		$("#alertList:empty").html("<h4>All Device Servers are ON</h4>");
		alertTimeout = setTimeout(getAlertList,5000);
	})
}
