# elettrApp
Design of a hybrid app for mobile devices to interface Elettra control system and other features.

# Requirements
- multiplatform
- fast development

## Multiplatform
We considered the possibility to build a hybrid app more attractive than a native app for Android because hybrid apps can run on all the main mobile platforms and on the web.

## Fast development
Most of the development was done by a bachelor's degree student as his thesis. The time available wasn't much longer than two months.
We expected to develop an app equivalent to a single synoptic panel already implemented as a native GUI (Graphic User Interface) written in C++ and Qt, 
but hybrid apps development was so quick that allowed to include a few other screens, one of them much more complicated.

# Technology
- Apache Cordova
- jQuery
- Bootstrap

## Apache Cordova
Apache Cordova allows to develop hybrid apps which means "Hybrid apps embed a mobile web site inside a native app. [...] This allows development using web technologies [...] while also retaining certain advantages of native apps (e.g. direct access to device hardware, offline operation, app store visibility)." (https://en.wikipedia.org/wiki/Web_application).
We used an excellent documentation (https://www.manning.com/books/apache-cordova-in-action) which allowed to be productive almost immediately.
Apache Cordova runs on the mobile browser, but the browser is hidden to the user so that the application looks like a native application.
The browser imply some inefficiency in comparison to native app, but the only sector interdicted is interactive gaming.
An other important feature of hybrid apps is the possibility to download from the web not only data but also templates (in form of HTML and JavaScript files).
This makes an app much more expandable and flexible (a proof of concept can be found at https://github.com/luciozambon/ProgressiveHybridApp).

## jQuery
jQuery is a cross-platform JavaScript library designed to simplify the client-side scripting of HTML (https://en.wikipedia.org/wiki/JQuery). 
jQuery contributed significantly to speed up the development time in respect to vanilla JavaScript (https://stackoverflow.com/questions/20435653/what-is-vanillajs).
jQuery is responsible of connecting asynchronously to a REST server. The connections are only client to server (polling).

## Bootstrap
Bootstrap is a free and open-source front-end web framework for designing websites and web applications. It contains HTML- and CSS-based design templates ( https://en.wikipedia.org/wiki/Bootstrap_(front-end_framework) ).
Bootstrap implements a mobile first design (https://en.wikipedia.org/wiki/Responsive_web_design). 
From the developer point of view, using Bootstrap requires little more effort than writing basic HTML, but the user experience is greatly improved.

## Ionic
We tried to use Ionic (https://en.wikipedia.org/wiki/Ionic_(mobile_app_framework)) but the benefits from this framework didn't come quickly enough so we aborted this part of our development.
Its later introduction points out the problem of rewriting the app built using jQuery using a totally different JavaScript framework, AngularJS (https://it.wikipedia.org/wiki/AngularJS) on which Ionic is based.

# Architecture
The app was developed as a Single-Page Application (https://en.wikipedia.org/wiki/Single-page_application), loading the resourses in response to user's actions, tring to give them the feeling of using a native app
The first screen is a basic starter composed by a button for each task plus a tick which make the task chosen the default.
The tasks are: synoptic status (for 2 accelerators and a complex system), cAstor, a starter administration tool (similar to TANGO Astor, for 3 accelerator domains) and shift calendar (for 2 groups).

## Synoptic status
All synoptic status share a common pattern.
Each synoptic screen is composed by one or two charts. The charts are always on top because our users asked to have them in evidence. 
Charts are embedded in an <iframe> tag using eGiga2m, a tool already used at Elettra (https://github.com/luciozambon/eGiga2m).
The <iframe> is a trivial way to use other applications as a component in a new one. This technique reduced dramatically the development time.
The price to pay is a certain inefficiency at run time, but in our case it was acceptable.
Below the charts there are a few boxes which are by default aligned vertically and closed on small devices, opened and in up to 4 columns on large devices.
Each box contains a few data, each with its label. These data was retrieved periodically from the REST server.
During the development, a few tests have been carried out to find the best way to group the data together and find the optimal length of the timer used to poll them.

## cAstor screens
This is a modified version of TANGO Astor (http://www.esrf.eu/computing/cs/tango/tango_doc/tools_doc/astor_doc/index.html). 
The tree structure of Astor was implemented but most menu were simplified.
When a device server is switched on or off, a modal containing an <iframe> is opened.
In this <iframe> there is an authentication form provided by an external service.
There is a new view that allows to monitor only the stopped device servers.
An other extension consisted in browsing till the attribute level and installing an alarm when trans-passing a configurable threshold.
But this feature was aborted because it was based on a very consuming polling system.

## Shift calendars
Shift calendars are simple adaptive tables

